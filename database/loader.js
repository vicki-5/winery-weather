const { readFileSync } = require('fs');
const data = readFileSync('./data.json');
const db = require('../back-end/database');
const {static} = require('express');

const jsonData = JSON.parse(data);

//parse out coastal
for (let i = 0; i < jsonData.Coastal.length; i++) {
   let record = jsonData.Coastal[i];
   db.query('INSERT INTO wineries(id,name,type,location) VALUES ($1,$2,$3,$4)',[record.id, record.name, 'Coastal', record.geometry],
      (err, res) => {
         console.log(err, res);
   });
}

//parse out valley
for (let i = 0; i < jsonData.Valley.length; i++) {
   let record = jsonData.Valley[i];
   db.query('INSERT INTO wineries(id,name,type,location) VALUES ($1,$2,$3,$4)',[record.id, record.name, 'Valley', record.geometry],
      (err, res) => {
         console.log(err, res);
      });
}

//parse out southern
for (let i = 0; i < jsonData.Southern.length; i++) {
   let record = jsonData.Southern[i];
   db.query('INSERT INTO wineries(id,name,type,location) VALUES ($1,$2,$3,$4)',[record.id, record.name, 'Southern', record.geometry],
      (err, res) => {
         console.log(err, res);
      });
}


CREATE TABLE wineries (
	id integer not null,
	name varchar(100) not null,
	type varchar(100) not null,
	location json not null
);

# README #

This README covers setup of the winery-weather application.

### Set up ###
This application runs on node.js, with a react front end.

Initial setup:
Run npm install from both the front-end and back-end directories
Create a table in Postgres using the create-table.sql in the database directory.

Run npm run server from the back-end directory to start the application.
Run loader.js in the database directory to load the database table.

Verify the table has data, then stop the npm server.

Create a .env file at the root of the project with WEATHER_API_KEY variable.

Run npm run dev to launch both the back-end and front-end applications.


### Notes ###

* Api returns data for winery list and populates a dropdown in the display.
* Api does not work for getting weather at this time.  Pieces are there, but ran out of time to complete.

### Structure ###

* Front-end contains the react application to display the weather data.
* Back-end contains the api and database calls.
* Database contains the loader functionality.

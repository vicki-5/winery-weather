const db = require('../database');

class Wineries {
  static retrieveAll (callback) {
    db.query('SELECT name from wineries', (err, res) => {
      if (err.error)
        return callback(err);
      callback(res);
    });
  }

  static getCoordinates (name, callback) {
    db.query('SELECT location from wineries WHERE name=${name}', (err, res) => {
      if (err.error)
        return callback(err);
      console.log(res);
      callback(res);
    });
  }
}

module.exports = Wineries;

const {Pool} = require('pg');

const CONNECTION_STRING = process.env.DATABASE_URL || 'postgresql://postgres:postgres@localhost:5432/winery-weather';

class DbConn {
  constructor () {
    this._pool = new Pool({
      connectionString: CONNECTION_STRING
    });

    this._pool.on('error', (err, client) => {
      console.error('Unexpected connection error on PostgreSQL db.', err);
      process.exit(-1);
    });

  }

  query (query, ...args) {
    this._pool.connect((err, client, done) => {
      if (err) throw err;
      const params = args.length === 2 ? args[0] : [];
      const callback = args.length === 1 ? args[0] : args[1];

      client.query(query, params, (err, res) => {
        done();
        if (err) {
          console.log(err.stack);
          return callback({ error: 'Database error.' }, null);
        }
        callback({}, res.rows);
      });
    });

  }

  end () {
    this._pool.end();
  }
}

module.exports = new DbConn();

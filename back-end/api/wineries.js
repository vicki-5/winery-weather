const express = require('express');
const wineries = require('../models/wineries');

const router = express.Router();

router.get('/', (req, res) => {
  wineries.retrieveAll((err, wineries) => {
    if (err)
      return res.json(err);
    return res.json(wineries);
  });
});

module.exports = router;

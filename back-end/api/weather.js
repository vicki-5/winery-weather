const express = require('express');
const weather = require('../models/weather');
const wineries = require('../models/wineries');

const router = express.Router();

router.get('/:name', (req, res) => {
  const name = req.params.name;

  wineries.getCoordinates(name, (err, coords)=> {
    if (err)
      return res.json(err);

    return res.json(coords);
  });

  weather.retrieveByCoord(-122.97, 38.75, (err, weather) => {
    if (err)
      return res.json(err);
    return res.json(weather);
  });
});

module.exports = router;

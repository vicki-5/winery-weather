import React, { Component } from 'react';

import {
  Container,
  Navbar,
  NavbarBrand,
  Row,
  Jumbotron,
  FormGroup,
  Input,
  Col
} from 'reactstrap';

import Weather from './Weather';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
       weather: null,
       wineryList: [],
       wineryName: null
    };
  }

  getWineryList = () => {
    fetch('/api/wineries')
    .then(res => res.json())
    .then(res => {
      const wineryList = res.map(r => r.name);
      this.setState({ wineryList });
    });
  };

  getWeather = (name) => {
    fetch(`/api/weather/${name}`)
    .then(res => res.json())
    .then(weather => {
      console.log(weather);
      this.setState({ weather });
    });
  }

  handleChangeWinery = (e) => {
    this.getWeather(e.target.value);
  }

  componentDidMount () {
    this.getWineryList();
  }

  render() {
    return (
      <Container fluid className="centered">
        <Navbar dark color="dark">
          <NavbarBrand href="/">Winery Weather</NavbarBrand>
        </Navbar>
        <Row>
          <Col>
            <Jumbotron>
              <h1 className="display-3">Winery Weather</h1>
              <p className="lead">The current weather for your favorite winery!</p>

            </Jumbotron>
          </Col>
        </Row>
        <Row>
          <Col>
            <h1 className="display-5">Current Weather</h1>
            <FormGroup>
              <Input type="select" onChange={this.handleChangeWinery}>
                { this.state.wineryList.length === 0 && <option>No wineries added yet.</option> }
                { this.state.wineryList.length > 0 && <option>Select a winery...</option> }
                { this.state.wineryList.map((name, i) => <option key={i}>{name}</option>) }
              </Input>
            </FormGroup>
          </Col>
        </Row>
        <Weather data={this.state.weather}/>
      </Container>
    );
  }
}

export default App;
